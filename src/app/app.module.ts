/***Default Modules***/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

/****third party modules***/
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
// import { DataTablesModule } from 'angular-datatables';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Router, Routes } from '@angular/router';

/***Default Component***/
import { AppComponent } from './app.component';
/***Custome Componets***/
import { HeaderComponent } from './components/lib/header/header.component';
import { FooterComponent } from './components/lib/footer/footer.component';
import { ProCarouselComponent } from './components/pro-carousel/pro-carousel.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ProductComponent } from './components/product/product.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { RegisterComponent } from './components/register/register.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { DetailsComponent } from './components/details/details.component';

/****CUSTOME SERVICES****/
import { FootwareServiceService } from './service/footware-service.service';
import { AdminLoginComponent } from './components/adminPanel/admin-login/admin-login.component';
import { AdminProductComponent } from './components/adminPanel/admin-product/admin-product.component';
import { AdminAnimationPageComponent } from './components/adminPanel/admin-animation-page/admin-animation-page.component';
import { AdminDashboardComponent } from './components/adminPanel/admin-dashboard/admin-dashboard.component';
import { CartComponent } from './components/cart/cart.component';
import { CategoryComponent } from './components/category/category.component';

/***All Custome Serivece***/

/***Custome Routing Concept***/

const routes:Routes = [
  { path:'', redirectTo: '/index', pathMatch: 'full' },
  { path: 'index', component:ProCarouselComponent },
  { path: 'product', component:ProductComponent },
  { path: 'checkout', component:CheckoutComponent },
  { path: 'register', component:RegisterComponent },
  { path: 'aboutus', component:AboutusComponent },
  { path: 'contactus', component:ContactusComponent },
  { path: 'details', component:DetailsComponent },
  { path: 'admin', component:AdminLoginComponent },
  { path: 'dashboard', component:AdminDashboardComponent },
  { path: 'adminproduct', component:AdminProductComponent },
  { path: 'cart', 
  component:CartComponent 
  },
  { path: 'category', 
  component:CategoryComponent 
  }

]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProCarouselComponent,
    CarouselComponent,
    ProductComponent,
    CheckoutComponent,
    RegisterComponent,
    ContactusComponent,
    AboutusComponent,
    DetailsComponent,
    AdminLoginComponent,
    AdminProductComponent,
    AdminAnimationPageComponent,
    AdminDashboardComponent,
    CartComponent,
    CategoryComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ReactiveFormsModule
    
  ],
  providers: [FootwareServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
