import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAnimationPageComponent } from './admin-animation-page.component';

describe('AdminAnimationPageComponent', () => {
  let component: AdminAnimationPageComponent;
  let fixture: ComponentFixture<AdminAnimationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAnimationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAnimationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
