import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FootwareServiceService } from '../../service/footware-service.service';
import {
  FormGroup, FormArray, FormBuilder, FormControl,
  Validators, ReactiveFormsModule
} from '@angular/forms';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder, private footservice:FootwareServiceService, private toastrservice:ToastrService) { }

  registrationForm:FormGroup;

  name: FormControl;
  email:FormControl;
  phone:FormControl;
  password:FormControl;


  ngOnInit() {
    this.buildRegistrationForm();

    
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  registrationFormSubmit(){
    console.log('INSIDE', this.registrationForm.controls);
    if (!(this.registrationForm.valid)) {
      this.validateAllFormFields(this.registrationForm);
    }
    else{
     let registerFormData = {
        name:this.name.value,
        email:this.email.value,
        phone:this.phone.value,
        password:this.password.value
      }
    console.log('registerFormData', registerFormData);
    this.footservice.getRegiserFormData(registerFormData)
    .subscribe(response =>{
      if(response.Code == 200){
        this.toastrservice.success("You Have Registered SuccessFul!");
        this.registrationForm.reset();
      }
      else{
        this.toastrservice.error("This Serivce Undermaintainence, Please Try Sometime!");
      }
    }) 
    }

  }

  buildRegistrationForm() {

    this.name = new FormControl("", [Validators.required, Validators.minLength(5)]),
    this.email = new FormControl("", [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z-]+.[a-zA-Z0-9-.]+$')]),
    this.phone = new FormControl("", [Validators.minLength(10),
    Validators.pattern('^[2 | 6-9][0-9]*$')]
    ),
    this.password = new FormControl("", [Validators.required, Validators.minLength(6)])

    this.registrationForm = new FormGroup({  
      name: this.name,
      email:this.email,
      phone:this.phone,
      password:this.password
    })
  } 
  
  

}
