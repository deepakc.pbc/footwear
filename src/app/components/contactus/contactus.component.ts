import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FootwareServiceService } from '../../service/footware-service.service';
import {
  FormGroup, FormArray, FormBuilder, FormControl,
  Validators, ReactiveFormsModule
} from '@angular/forms';
import * as $ from 'jquery';
import { Http } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  constructor( private footservice:FootwareServiceService, private toastrservice:ToastrService) { }

  contactForm:FormGroup;

  cname:FormControl;
  cemail:FormControl;
  cphone:FormControl;
  cmessage:FormControl;



  ngOnInit() {
    this.buildContactForm();
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  buildContactForm(){
    this.cname = new FormControl("", [Validators.required, Validators.minLength(5)]),
    this.cemail = new FormControl("", [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z-]+.[a-zA-Z0-9-.]+$')]),
    this.cphone = new FormControl("", [Validators.required,
    Validators.pattern('^[6-9][0-9]*$')]
    ),
    this.cmessage = new FormControl("")

    this.contactForm = new FormGroup({  
      cname: this.cname,
      cemail:this.cemail,
      cphone:this.cphone,
      cmessage:this.cmessage
    })
  }

  contactFormSubmit(){
    console.log(this.contactForm.controls);
    if(!(this.contactForm.valid)){
      this.validateAllFormFields(this.contactForm);
    }
    else{
      let contactFormData = {
        cname:this.cname.value,
        cemail:this.cemail.value,
        cphone:this.cphone.value,
        cmessage:this.cmessage.value
      }

      this.footservice.getContactFormData(contactFormData)
      .subscribe(response =>{
        if(response.Code == 200){
          this.toastrservice.success("Thanks For Contact Us We Will Contact You Soon...");
          this.contactForm.reset();
        }
        else{
          this.toastrservice.error("This Serivce Undermaintainence, Please Try Sometime!");
        }
      })
    }
  }
}
