import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
// import { environment } from '../../environments/environment'

@Injectable()
export class FootwareServiceService {

  private insertRegisterApi =  'http://13.232.74.116:4000/common/register';

  private insertContactApi =  'http://localhost:3000/common/contact';

  constructor(private _http:Http) { }

  getRegiserFormData(inputData){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.insertRegisterApi, inputData, options).map((res: Response) => res.json())
  }

  getContactFormData(inputData){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.insertContactApi, inputData, options).map((res: Response) => res.json())
  }
}
